#ifndef MOVELOGICFUNCTOR_H
#define MOVELOGICFUNCTOR_H

#include <QDebug>
#include "Platform.h"

extern int SCREEN_HEIGHT;
extern int SCREEN_WIDTH;



/** @class MoveLogicFunctor<T>
 * @brief This functor encapsulates the logic for character movement on the X and Y axes.
 * @tparam T The class that contains the MoveLogicFunctor<T> object as a member variable.
 *
 * To work, this class requires the following member variables to be defined in the class "T":
 * (1) bool moving;
 * (2) Platform* platformStandingOn;
 * Also, the class "T" must declare MoveLogicFunctor<T> as a "friend class" so as to allow access to private member variables
 *
 */
template <typename T>
class MoveLogicFunctor {
public:
    MoveLogicFunctor();
    virtual ~MoveLogicFunctor();

    void operator()(T& objToModify);
};




/** ---------------------- RECALL: Must define TEMPLATE functions etc. in a header since they aren't "real" classes! ---------------------- **/

template <typename T>
MoveLogicFunctor<T>::MoveLogicFunctor() {

}

template<typename T>
MoveLogicFunctor<T>::~MoveLogicFunctor() {

}

template <typename T>
void MoveLogicFunctor<T>::operator()(T& objToModify) {

    float& velocityX = objToModify.velocityX;
    float& velocityY = objToModify.velocityY;

    float x_current = objToModify.pos().x();
    float y_current = objToModify.pos().y();
    float x_new = x_current + velocityX;
    float y_new = y_current + velocityY;
    float minX = 0;
    float maxX = SCREEN_WIDTH - objToModify.boundingRect().width();

    if (velocityX > 0) {  // If moving right...
        objToModify.moving = true;
        if (x_new < maxX) {  // ... and player isn't hitting the wall...
            objToModify.setPos(x_new, y_current);  // ... set the player's position to x_pos+dx
        } else {
            objToModify.setPos(maxX, y_current);
        }
    } else if (velocityX < 0) {
        objToModify.moving = true;
        if (x_new > minX) {
            objToModify.setPos(x_new, y_current);
        } else {
            objToModify.setPos(minX, y_current);
        }
    } else {
        objToModify.moving = false;
    }

    // Check if user is walking off of platform
    Platform*& platformStandingOn = objToModify.platformStandingOn;
    if (objToModify.moving && platformStandingOn != nullptr) {
        float playerMiddle_X = objToModify.pos().x() + (objToModify.boundingRect().width() / 2);
        float platformStandingOn_minX = platformStandingOn->rect().x();
        float platformStandingOn_maxX = (platformStandingOn_minX + platformStandingOn->rect().width());
        if (playerMiddle_X >= platformStandingOn_minX && playerMiddle_X <= platformStandingOn_maxX ) {
            return;
        } else {
            platformStandingOn = nullptr;
        }
    }

    // Check for if the player is in the air or on a platform
    if (platformStandingOn == nullptr) {  // Upon pressing JUMP, player's platformStandingOn = nullptr because not on a platform in the air!

        float gravity = 2;

        if (velocityY < 0) {  // If moving up, disregard any platforms and set player's position to y_pos+dx
            objToModify.setPos(objToModify.pos().x(), y_new);
            velocityY += gravity;  // Mimic gravity: slow the player until he/she starts to fall back to the ground after jumping
        } else if (velocityY >= 0) {  // If moving down (or stationary), check for if player is on a platform

            // First, update the player's position to check for future collision with platform
            objToModify.setPos(objToModify.pos().x(), y_new);

            // Check if on platform by getting list of colliding items...
            QList<QGraphicsItem*> potentialPlatforms = objToModify.collidingItems();
            size_t i = 0;
            for (size_t end = potentialPlatforms.size(); i < end; ++i) {
                Platform* platform_possible = dynamic_cast<Platform*>(potentialPlatforms[i]);
                if (platform_possible != nullptr) {  // If item colliding with is a platform, stop acceleration
                    float playerFeet_i = y_current + objToModify.boundingRect().height();  // Location of player's feet before shifting y-position
                    float playerFeet_f = y_new + objToModify.boundingRect().height();      // ... after shifting y-position
                    float platformTop = platform_possible->rect().y();
                    float playerMiddle_X = objToModify.pos().x() + (objToModify.boundingRect().width() / 2);
                    float platform_possible_minX = platform_possible->rect().x();
                    float platform_possible_maxX = (platform_possible_minX + platform_possible->rect().width());
                    // If user's feet will hit platform upon move AND the user's middle is on some part (x-pos) of the platform, place on platform.
                    if ( (playerFeet_i <= platformTop && playerFeet_f >= platformTop) && (playerMiddle_X >= platform_possible_minX && playerMiddle_X <= platform_possible_maxX) ) {
                        platformStandingOn = platform_possible;
                        break;
                    } else {
                        platformStandingOn = nullptr;
                    }
                }
            }

            // If on platform, stop descent; else, keep descending and increase fall velocity
            if (platformStandingOn != nullptr) {
                y_new = platformStandingOn->boundingRect().y() - objToModify.boundingRect().height();
                objToModify.setPos(objToModify.pos().x(), y_new + 1);
                velocityY = 0;
            } else {
                objToModify.setPos(objToModify.pos().x(), y_new);
                velocityY += gravity;
            }
        }
    }

}



#endif // MOVELOGICFUNCTOR_H
