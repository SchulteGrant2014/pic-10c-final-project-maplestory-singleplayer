#ifndef MYRECT_H
#define MYRECT_H

#include <QGraphicsPixmapItem>
#include <QTimer>
#include <set>
#include "Platform.h"
#include "Ladder.h"
#include "MoveLogicFunctor.h"


class MyRect : public QObject, public QGraphicsPixmapItem {

    Q_OBJECT

public:
    MyRect();
    MyRect(const MyRect& a);  // Copy Constructor
    MyRect(MyRect&& a);  // Move Constructor
    MyRect& operator=(MyRect a);  // Assignment Operator
    void swap(MyRect& a);
    virtual ~MyRect();

    void keyPressEvent(QKeyEvent* e);
    void keyReleaseEvent(QKeyEvent *e);

    void stopMoveTimer();
    void startMoveTimer(int msec);

    void stopEnemyCollisionTimer();
    void startEnemyCollisionTimer(int msec);
    void startCollisionCooldown();

    inline void setVelocityX(int v) { velocityX = v; }
    inline void setVelocityY(int v) { velocityY = v; }
    inline int getVelocityX() const { return velocityX; }
    inline int getVelocityY() const { return velocityY; }
    void jump();
    void set_moving(bool a) { moving = a; }
    bool get_moving() { return moving; }
    Platform*& get_platformStandingOn() { return platformStandingOn; }

public slots:
    void move();
    void checkForEnemyCollision();
    void endCollisionCooldown();

private:
    float velocityX;
    float velocityY;
    std::set<int> keysDown;  // Keeps track of all keys pressed
    int directionFacing;  // 0 = Left, 1 = Right
    QTimer* moveTimer;
    QTimer* enemyCollisionTimer;
    QTimer* collisionCooldownTimer;
    int moveTimer_refreshRate;
    bool moving;
    bool collisionCooldown;
    Platform* platformStandingOn;  // Memory handled by Qt
    Ladder* ladderStandingOn;      // Memory handled by Qt
    MoveLogicFunctor<MyRect> moveLogicFnctr;
    void (*moveLogic_ladder)(MyRect*);

    friend class MoveLogicFunctor<MyRect>;
    friend void ladderMoveLogic(MyRect* player);
};



#endif // MYRECT_H
