#include "Stump.h"
#include "Enemy.h"

extern Animations* animations;

Stump::Stump() : Enemy(animations->stump_move[0], false, 3, 10) {
    frameCounter_move.setFrameTotal(animations->stump_move.size());
    frameCounter_stand.setFrameTotal(animations->stump_stand.size());
}

Stump::~Stump() {

}

void Stump::updateFrame_move(size_t frameIndex) {
    setPixmap(animations->stump_move[frameIndex]);
}

void Stump::updateFrame_stand(size_t frameIndex) {
    setPixmap(animations->stump_stand[frameIndex]);
}

void Stump::updateFrame_jump() {
    // Can't jump... just set to standing
    setPixmap(QString(":/Mobs/Mobs/Stump/stand_0.png"));
}
