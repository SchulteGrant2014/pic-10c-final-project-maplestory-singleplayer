
#include <QTimer>
#include <QGraphicsScene>  // For using SCENE functions
#include <QList>
#include <QGraphicsItem>
#include "Enemy.h"
#include "Game.h"
#include "Animations.h"
#include <cstdlib>

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern Game* game;

Enemy::Enemy(QString filePath, bool canJump, int speed, int attack) : QObject(), QGraphicsPixmapItem(), canJump(canJump), speed(speed), attack(attack), moveDecisionTimer(nullptr), moveTimer(nullptr), platformStandingOn(nullptr), velocityY(0), moving(false), frameCounter_move(0), frameCounter_stand(0), animationBuffer(0) {

    this->setPixmap(filePath);

    // Start enemy movement
    {
        int randNum = std::rand() % 3;
        if (randNum == 0) {
            velocityX = speed;
        } else if (randNum == 1) {
            velocityX = -speed;
        } else {
            velocityX = 0;
        }
    }

    // Set enemy spawn position to be on the top of some platform
    {
        int randomPlatformIndex = std::rand() % (game->platforms.size());
        int x_posMin = game->platforms[randomPlatformIndex]->boundingRect().x();
        int x_posMax = x_posMin + game->platforms[randomPlatformIndex]->boundingRect().width() - this->boundingRect().width();
        int x_posRand = x_posMin + (std::rand() % (x_posMax - x_posMin));
        int y_pos = game->platforms[randomPlatformIndex]->boundingRect().y() - this->boundingRect().height();
        this->setPos(x_posRand, y_pos);
        this->platformStandingOn = game->platforms[randomPlatformIndex];
    }

    // After 1 second, the enemy will decide if it wants to keep its current move status, start moving, stop moving, or change directions
    startMoveDecisionTimer(1000);
    startMoveTimer(35);
}

Enemy::Enemy(const Enemy &a) : moveDecisionTimer(nullptr), moveTimer(nullptr), frameCounter_move(a.frameCounter_move), frameCounter_stand(a.frameCounter_stand), animationBuffer(0) {
    // Manage basic member variable values...
    canJump = a.canJump;
    speed = a.speed;
    attack = a.attack;
    platformStandingOn = a.platformStandingOn;
    moveLogicFnctr = a.moveLogicFnctr;
    velocityX = a.velocityX;
    velocityY = a.velocityY;
    moving = a.moving;

    // Manage heap memory...
    try {
        moveTimer = new QTimer;
    } catch (std::exception& e) {
        delete moveTimer;
        moveTimer = nullptr;
        throw;
    }

    QObject::connect(moveTimer, SIGNAL(timeout()), this, SLOT(move()));

    try {
        moveDecisionTimer = new QTimer;
    } catch (std::exception& e) {
        delete moveDecisionTimer;
        moveDecisionTimer = nullptr;
        throw;
    }

    QObject::connect(moveDecisionTimer, SIGNAL(timeout()), this, SLOT(move()));
}

Enemy::Enemy(Enemy&& a) : frameCounter_move(0), frameCounter_stand(0) {
    this->swap(a);
}
/*
Enemy& Enemy::operator=(Enemy a) {
    this->swap(a);
    return *this;
}
*/
void Enemy::swap(Enemy& a) {
    std::swap(canJump, a.canJump);
    std::swap(speed, a.speed);
    std::swap(attack, a.attack);
    std::swap(moveDecisionTimer, a.moveDecisionTimer);
    std::swap(moveTimer, a.moveTimer);
    std::swap(platformStandingOn, a.platformStandingOn);
    std::swap(moveLogicFnctr, a.moveLogicFnctr);
    std::swap(velocityX, a.velocityX);
    std::swap(velocityY, a.velocityY);
    std::swap(moving, a.moving);
    std::swap(frameCounter_move, a.frameCounter_move);
}

Enemy::~Enemy() {
    delete moveDecisionTimer;  moveDecisionTimer = nullptr;
    delete moveTimer;          moveTimer = nullptr;
}

void Enemy::stopMoveDecisionTimer() {
    delete moveDecisionTimer;
    moveDecisionTimer = nullptr;
}

void Enemy::startMoveDecisionTimer(int msec) {
    // If timer already exists, get rid of it and make a new one
    delete moveDecisionTimer;

    // Create enemies periodically
    try {
        moveDecisionTimer = new QTimer;
    } catch (std::exception& e) {
        delete moveDecisionTimer;
        moveDecisionTimer = nullptr;
        throw;
    }

    QObject::connect(moveDecisionTimer, SIGNAL(timeout()), this, SLOT(decideChangeVelocity()));
    moveDecisionTimer->start(msec);
}

void Enemy::stopMoveTimer() {
    delete moveTimer;
    moveTimer = nullptr;
}

void Enemy::startMoveTimer(int msec) {
    // If timer already exists, get rid of it and make a new one
    delete moveTimer;

    // Move the enemy entity periodically
    try {
        moveTimer = new QTimer;
    } catch (std::exception& e) {
        delete moveTimer;
        moveTimer = nullptr;
        throw;
    }

    QObject::connect(moveTimer, SIGNAL(timeout()), this, SLOT(move()));
    moveTimer->start(msec);
}

void Enemy::changeVelocityRand() {
    if (velocityX == 0) {  // If not moving, start moving some direction
        if (std::rand() % 2) {
            velocityX = speed;
        } else {
            velocityX = -speed;
        }
    } else {  // If already moving, stop moving entirely OR change the direction of travel
        if ((std::rand() % 3) == 0) {
            velocityX = 0;
        } else {
            velocityX *= -1;
        }
    }
}

void Enemy::changeVelocity(int velX, int velY) {
    velocityX = velX;
    velocityY = velY;
}

void Enemy::jump() {
    velocityY = -15;
    platformStandingOn = nullptr;
}

void Enemy::inflictDamage() {
    game->decrease_health(attack);  // Inflict damage on player based on Enemy's attack stat
}

void Enemy::move() {

    // Update animation frames based on what the monster is doing at the current instant
    if (velocityX != 0 && velocityY == 0) {
        if (animationBuffer >= 4) {
            updateFrame_move(++frameCounter_move);
            animationBuffer = 0;
        } else {
            ++animationBuffer;
        }
        frameCounter_stand = 0;
    } else if (velocityX == 0 && velocityY == 0) {
        if (animationBuffer >= 4) {
            updateFrame_stand(++frameCounter_stand);
            animationBuffer = 0;
        } else {
            ++animationBuffer;
        }
        frameCounter_move = 0;
    }

    // Call functor containing the movement logic to update Enemy position
    moveLogicFnctr(*this);

    // If on left edge of platform, turn enemy around OR jump off
    if (platformStandingOn != nullptr) {
        int platform_xMin = platformStandingOn->boundingRect().x();
        int platform_xMax = platform_xMin + platformStandingOn->boundingRect().width();
        int enemy_xLeft = this->pos().x();
        int enemy_xLeftNew = enemy_xLeft + velocityX;
        int enemy_xRight = enemy_xLeft + this->boundingRect().width() + 1;
        int enemy_xRightNew = enemy_xRight + velocityX;
        if ( (enemy_xLeft >= platform_xMin && enemy_xLeftNew <= platform_xMin) ||
             (enemy_xRight <= platform_xMax && enemy_xRightNew >= platform_xMax) ) {
            if (std::rand() % 8 == 0 && canJump) {  // Once in 8 times, the enemy will jump off of the platform
                jump();
            } else {
                velocityX *= -1;  // Turn the enemy around so it keeps moving instead of stopping
            }
        }
    }

    // Y-axis: Every so often, the enemy will jump
    if ( ((std::rand() % 1000) < 10) && platformStandingOn != nullptr && canJump ) {
        jump();
        updateFrame_jump();
    }
}

void Enemy::decideChangeVelocity() {
    if (velocityY == 0 && (std::rand() % 3) == 0) { // On average, the enemy will change its velocity somehow after 4 seconds if not falling, etc.
        this->changeVelocityRand();
    }
}










