#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsTextItem>
#include <QMediaPlayer>
#include <QTimer>
#include <vector>
#include "MyRect.h"
#include "Enemy.h"
#include "Score.h"
#include "Health.h"
#include "Platform.h"
#include "Ladder.h"

class Game : public QObject {
    Q_OBJECT
public:
    explicit Game();
    virtual ~Game();

    Game(const Game& a) = delete;  // Shouldn't ever make a copy of the game! There is only one game.
    Game(Game&& a) = delete;  // Shouldn't ever make a copy of the game! There is only one game.
    Game& operator=(Game a) = delete;  // Shouldn't ever make a copy of the game! There is only one game.

    void startSpawnTimer(int msec);
    void stopSpawnTimer();

    void spawnSingleEnemy();
    void killAllEnemies();

    Platform* add_platform_toScene(int x, int y, int w, int h);
    Ladder* add_ladder_toScene(int x, int y, int w, int h);

    void increase_score(int n);
    inline int get_score() { return score->get_score(); }

    void decrease_health(int n);
    inline int get_health() { return health->get_health(); }

    void gameOver(bool won);
    void displayGameOverWindow(QString message);

public slots:
    void spawnEnemy_slot();
    void pause();

private:
    MyRect* player;
    QGraphicsScene* scene;
    QGraphicsView* view;
    QTimer* spawnTimer;
    std::vector<Platform*> platforms;
    std::vector<Ladder*> ladders;
    std::vector<Enemy*> enemies;
    QMediaPlayer* music;
    Score* score;
    Health* health;
    QGraphicsTextItem* gameOverText;

    friend class Enemy;
    friend class Bullet;
};

#endif // GAME_H
