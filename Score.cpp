#include <QGraphicsTextItem>
#include <QFont>
#include <QString>
#include "Score.h"

Score::Score(int n) : QGraphicsTextItem(), score(n) {
    setPlainText(QString("Score: ") + QString::number(score));
    setDefaultTextColor(QColor(0, 0, 255));
    setFont(QFont("Times", 24));
}

Score::~Score() {

}

void Score::increase_score(int n) {
    score += n;
    setPlainText(QString("Score: ") + QString::number(score));
}
