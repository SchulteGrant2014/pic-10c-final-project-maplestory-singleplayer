#include "Slime.h"
#include "Enemy.h"

extern Animations* animations;

Slime::Slime() : Enemy(animations->slime_move[0], true, 5, 10) {
    frameCounter_move.setFrameTotal(animations->slime_move.size());
    frameCounter_stand.setFrameTotal(animations->slime_stand.size());
}

Slime::~Slime() {

}

void Slime::updateFrame_move(size_t frameIndex) {
    setPixmap(animations->slime_move[frameIndex]);
}

void Slime::updateFrame_stand(size_t frameIndex) {
    setPixmap(animations->slime_stand[frameIndex]);
}

void Slime::updateFrame_jump() {
    setPixmap(QString(":/Mobs/Mobs/Slime/jump_0.png"));
}
