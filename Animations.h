#ifndef ANIMATIONS_H
#define ANIMATIONS_H

#include <vector>
#include <QString>

class Animations {
    typedef std::vector<QString> FRAMES;
public:
    Animations();
    Animations(const Animations& a) = delete;
    Animations& operator=(Animations) = delete;

    // Keep all animation frames public for easy access
    FRAMES bullet_move;

    FRAMES slime_move;
    FRAMES slime_stand;

    FRAMES orangeMushroom_move;
    FRAMES orangeMushroom_stand;

    FRAMES greenMushroom_move;
    FRAMES greenMushroom_stand;

    FRAMES ribbonPig_move;
    FRAMES ribbonPig_stand;

    FRAMES stump_move;
    FRAMES stump_stand;

};

#endif // ANIMATIONS_H
