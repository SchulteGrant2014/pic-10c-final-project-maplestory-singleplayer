#ifndef HEALTH_H
#define HEALTH_H

#include <QGraphicsTextItem>

class Health : public QGraphicsTextItem {
public:
    Health(int n = 100);
    virtual ~Health();

    void decrease_health(int n);
    inline int get_health() { return health; }

private:
    int health;
};

#endif // HEALTH_H
