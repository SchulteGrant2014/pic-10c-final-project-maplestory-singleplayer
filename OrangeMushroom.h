#ifndef ORANGEMUSHROOM_H
#define ORANGEMUSHROOM_H

#include "Enemy.h"

class OrangeMushroom : public Enemy {
public:
    OrangeMushroom();
    virtual ~OrangeMushroom();

    virtual void updateFrame_move(size_t frameIndex);
    virtual void updateFrame_stand(size_t frameIndex);
    virtual void updateFrame_jump();
};

#endif // ORANGEMUSHROOM_H
