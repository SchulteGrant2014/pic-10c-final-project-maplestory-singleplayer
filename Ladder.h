#ifndef LADDER_H
#define LADDER_H

#include <QGraphicsRectItem>

/** @class Ladder
 * @brief Ladders allow the player to ascend or descend without the effects of gravity
 *
 * The Ladder class allows creation of game objects that the player can attach to and negate the effects of gravity.
 * This class only encapsulates the bounds of the ladder, as well as functions that return whether the player is at the top/bottom of the ladder or climbing in the middle somewhere.
 * Note: This means that all Ladder ascent/descent/attachment/detachment logic must be defined for the individual player/entity class that will make use of the ladder.
 */
class Ladder : public QGraphicsRectItem {
public:
    Ladder(int x = 0, int y = 0, int w = 0, int h = 0);
    virtual ~Ladder();
    bool atTop(QGraphicsPixmapItem* a);
    bool atBottom(QGraphicsPixmapItem* a);

};

#endif // LADDER_H
