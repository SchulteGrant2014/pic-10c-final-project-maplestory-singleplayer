#ifndef SLIME_H
#define SLIME_H

#include "Enemy.h"

class Slime : public Enemy {
public:
    Slime();
    virtual ~Slime();

    virtual void updateFrame_move(size_t frameIndex);
    virtual void updateFrame_stand(size_t frameIndex);
    virtual void updateFrame_jump();
};

#endif // SLIME_H
