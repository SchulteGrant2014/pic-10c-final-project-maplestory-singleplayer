#ifndef SCORE_H
#define SCORE_H

#include <QGraphicsTextItem>

class Score : public QGraphicsTextItem {
public:
    Score(int n = 0);
    virtual ~Score();

    void increase_score(int n);
    inline int get_score() { return score; }

private:
    int score;

};

#endif // SCORE_H
