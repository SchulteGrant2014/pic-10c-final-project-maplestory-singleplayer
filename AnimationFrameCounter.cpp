#include "AnimationFrameCounter.h"

AnimationFrameCounter::AnimationFrameCounter(size_t vecSize) : frameCurrent(0), frameTotal(vecSize){

}

AnimationFrameCounter::~AnimationFrameCounter() {

}

size_t& AnimationFrameCounter::operator++() {
    if (++frameCurrent >= frameTotal) {  // Increment current frame index
        frameCurrent = 0;  // If incremented past the total number of frame indices, reset the frame loop to 0
    }
    return frameCurrent;
}

size_t AnimationFrameCounter::operator++(int) {
    AnimationFrameCounter copy = *this;
    ++(*this);
    return *copy;
}

size_t AnimationFrameCounter::operator*() {
    return frameCurrent;
}


