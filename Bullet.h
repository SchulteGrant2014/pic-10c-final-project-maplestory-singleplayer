#ifndef BULLET_H
#define BULLET_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QTimer>
#include "AnimationFrameCounter.h"

class Bullet : public QObject, public QGraphicsPixmapItem {

    Q_OBJECT

public:
    Bullet(int direction);  // Normal Constructor
    Bullet(const Bullet& a);  // Copy Constructor
    Bullet(Bullet&& a);  // Move Constructor
    Bullet& operator=(Bullet a);  // Assignment Operator
    void swap(Bullet& a);
    virtual ~Bullet();

public slots:
    void move();

private:
    QTimer* moveTimer;
    int direction;  // 0 = Left, 1 = Right
    int speed;
    AnimationFrameCounter animFrame;

};


#endif // BULLET_H
