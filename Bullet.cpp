#include <QTimer>
#include <QGraphicsScene>  // For using SCENE functions
#include <QList>
#include <vector>
#include "Bullet.h"
#include "Enemy.h"
#include "Game.h"
#include "Animations.h"

extern Game* game;
extern Animations* animations;

Bullet::Bullet(int direction) : QObject(), QGraphicsPixmapItem(), moveTimer(nullptr), direction(direction), speed(15), animFrame(animations->bullet_move.size()) {

    setPixmap(animations->bullet_move[0]);

    // Connect the Bullet object to a timer so that it will move periodically: #include<QTimer>
    try {
        moveTimer = new QTimer;
    } catch (std::exception& e) {
        delete moveTimer;
        moveTimer = nullptr;
        throw;
    }

    connect(moveTimer, SIGNAL(timeout()), this, SLOT(move()));
    moveTimer->start(50);  // The timer goes off every 50 milliseconds. Every 50 milliseconds, the bullet will move.
}

Bullet::Bullet(const Bullet& a) : animFrame(a.animFrame) {
    direction = a.direction;
    speed = a.speed;
    try {
        moveTimer = new QTimer;
    } catch (std::exception& e) {
        delete moveTimer;
        moveTimer = nullptr;
        throw;
    }
}

Bullet::Bullet(Bullet&& a) : animFrame(0) {  // animFrame will be swapped out anyways. Don't take the time to dereference...
    this->swap(a);
}

Bullet& Bullet::operator=(Bullet a) {
    this->swap(a);
    return *this;
}

void Bullet::swap(Bullet& a) {
    std::swap(direction, a.direction);
    std::swap(speed, a.speed);
    std::swap(animFrame, a.animFrame);
    std::swap(moveTimer, a.moveTimer);
}

Bullet::~Bullet() {
    delete moveTimer;  moveTimer = nullptr;
}


void Bullet::move() {

    setPixmap(animations->bullet_move[animFrame++]);

    QList<QGraphicsItem*> colliding = collidingItems();
    for (size_t i = 0, numItems = colliding.size(); i < numItems; ++i) {
        if (dynamic_cast<Enemy*>(colliding[i]) != nullptr) {  // If item colliding with is an enemy, delete enemy + bullet
            scene()->removeItem(colliding[i]);
            scene()->removeItem(this);
            // If bullet collides with target enemy, delete the enemy from the game's vector of enemies
            std::vector<Enemy*>& enemyVecRef = game->enemies;
            auto enemyFind_it = find(enemyVecRef.begin(), enemyVecRef.end(), colliding[i]);
            if (enemyFind_it != enemyVecRef.end()) {
                delete *enemyFind_it;
                enemyVecRef.erase(enemyFind_it);
            }
            delete this;
            game->increase_score(1);
            return;
        }
    }

    // Check if the bullet went off the screen. If so, DELETE IT to conserve resources.
    if ( (this->pos().x() + this->boundingRect().width()) <= 0) {
        scene()->removeItem(this);  // Remove the item from the scene...
        delete this;                // ... then delete it.
    } else {
        if (direction == 0) {  // If will travel Left (0)...
            this->setPos(x() - speed, y());
        } else {  // If will travel Right (1)...
            this->setPos(x() + speed, y());
        }
    }
}
