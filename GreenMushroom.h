#ifndef GREENMUSHROOM_H
#define GREENMUSHROOM_H

#include "Enemy.h"

class GreenMushroom : public Enemy {
public:
    GreenMushroom();
    virtual ~GreenMushroom();

    virtual void updateFrame_move(size_t frameIndex);
    virtual void updateFrame_stand(size_t frameIndex);
    virtual void updateFrame_jump();
};

#endif // GREENMUSHROOM_H
