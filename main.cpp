#include <QApplication>
#include <QGraphicsView>
#include <QTimer>
#include "MyRect.h"
#include "Bullet.h"
#include "Enemy.h"
#include "Game.h"
#include "Animations.h"

int SCREEN_WIDTH = 800;
int SCREEN_HEIGHT = 600;
Game* game = nullptr;
Animations* animations = nullptr;


int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    Animations anim_obj;
    animations = &anim_obj;

    Game game_obj;
    game = &game_obj;
    game->startSpawnTimer(5000);

    return a.exec();
}


