#ifndef RIBBONPIG_H
#define RIBBONPIG_H

#include "Enemy.h"

class RibbonPig : public Enemy {
public:
    RibbonPig();
    virtual ~RibbonPig();

    virtual void updateFrame_move(size_t frameIndex);
    virtual void updateFrame_stand(size_t frameIndex);
    virtual void updateFrame_jump();
};

#endif // RIBBONPIG_H
