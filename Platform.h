#ifndef PLATFORM_H
#define PLATFORM_H

#include <QGraphicsRectItem>

class Platform : public QGraphicsRectItem {
public:
    Platform(int x = 0, int y = 0, int w = 0, int h = 0);
    virtual ~Platform();
};

#endif // PLATFORM_H
