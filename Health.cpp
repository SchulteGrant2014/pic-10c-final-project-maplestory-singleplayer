#include <QGraphicsTextItem>
#include <QFont>
#include "Health.h"

Health::Health(int n) : QGraphicsTextItem(), health(n) {
    setPlainText(QString("Health: ") + QString::number(health));
    setDefaultTextColor(QColor(255, 0, 0));
    setFont(QFont("Times", 24));
    setPos(128, 0);
}

Health::~Health() {

}

void Health::decrease_health(int n) {
    health -= n;
    setPlainText(QString("Health: ") + QString::number(health));
}
