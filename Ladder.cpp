
#include "Ladder.h"


Ladder::Ladder(int x, int y, int w, int h) : QGraphicsRectItem() {
    setRect(0, 0, w, h);
    setPos(x, y);
}

Ladder::~Ladder() {

}

bool Ladder::atTop(QGraphicsPixmapItem* a) {
    int playerFeetPos = a->pos().y() + a->boundingRect().height();
    int ladderTop = this->pos().y();
    if (playerFeetPos < ladderTop) {
        return true;
    } else {
        return false;
    }
}

bool Ladder::atBottom(QGraphicsPixmapItem* a) {
    int playerMidPos = a->pos().y() + (a->boundingRect().height() / 3);
    int ladderBottom = this->pos().y() + this->rect().height();
    if (playerMidPos > ladderBottom) {
        return true;
    } else {
        return false;
    }
}

