#include <QGraphicsRectItem>
#include "Platform.h"
#include "Game.h"

Platform::Platform(int x, int y, int w, int h) : QGraphicsRectItem() {
    setRect(x, y, w, h);
}

Platform::~Platform() {

}
