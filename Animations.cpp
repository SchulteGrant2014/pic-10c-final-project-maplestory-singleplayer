#include "Animations.h"

Animations::Animations() {

    // Pre-set the size of each animation frame vector
    bullet_move.resize(4);
    slime_move.resize(7);
    slime_stand.resize(3);
    orangeMushroom_move.resize(3);
    orangeMushroom_stand.resize(2);
    greenMushroom_move.resize(4);
    greenMushroom_stand.resize(3);
    ribbonPig_move.resize(3);
    ribbonPig_stand.resize(3);
    stump_move.resize(4);
    stump_stand.resize(1);


    /* --------------------------------------- Bullet Animations --------------------------------------- */
    // Bullet Move frames
    bullet_move.resize(4, "");
    bullet_move[0] = QString(":/images/energyBolt_move_0");
    bullet_move[1] = QString(":/images/energyBolt_move_1");
    bullet_move[2] = QString(":/images/energyBolt_move_2");
    bullet_move[3] = QString(":/images/energyBolt_move_3");

    /* --------------------------------------- Slime Animations --------------------------------------- */
    // Slime Move frames
    slime_move[0] = QString(":/Mobs/Mobs/Slime/move_0.png");
    slime_move[1] = QString(":/Mobs/Mobs/Slime/move_1.png");
    slime_move[2] = QString(":/Mobs/Mobs/Slime/move_2.png");
    slime_move[3] = QString(":/Mobs/Mobs/Slime/move_3.png");
    slime_move[4] = QString(":/Mobs/Mobs/Slime/move_4.png");
    slime_move[5] = QString(":/Mobs/Mobs/Slime/move_5.png");
    slime_move[6] = QString(":/Mobs/Mobs/Slime/move_6.png");

    // Slime Stand frames
    slime_stand[0] = QString(":/Mobs/Mobs/Slime/stand_0.png");
    slime_stand[1] = QString(":/Mobs/Mobs/Slime/stand_1.png");
    slime_stand[2] = QString(":/Mobs/Mobs/Slime/stand_2.png");

    /* --------------------------------------- Orange Mushroom Animations --------------------------------------- */
    // Orange Mushroom Move frames
    orangeMushroom_move[0] = QString(":/Mobs/Mobs/Orange_Mushroom/move_0.png");
    orangeMushroom_move[1] = QString(":/Mobs/Mobs/Orange_Mushroom/move_1.png");
    orangeMushroom_move[2] = QString(":/Mobs/Mobs/Orange_Mushroom/move_2.png");

    // Orange Mushroom Stand frames
    orangeMushroom_stand[0] = QString(":/Mobs/Mobs/Orange_Mushroom/stand_0.png");
    orangeMushroom_stand[1] = QString(":/Mobs/Mobs/Orange_Mushroom/stand_1.png");

    /* --------------------------------------- Green Mushroom Animations --------------------------------------- */
    // Green Mushroom Move frames
    greenMushroom_move[0] = QString(":/Mobs/Mobs/Green_Mushroom/move_0.png");
    greenMushroom_move[1] = QString(":/Mobs/Mobs/Green_Mushroom/move_1.png");
    greenMushroom_move[2] = QString(":/Mobs/Mobs/Green_Mushroom/move_2.png");
    greenMushroom_move[3] = QString(":/Mobs/Mobs/Green_Mushroom/move_3.png");

    // Green Mushroom Stand frames
    greenMushroom_stand[0] = QString(":/Mobs/Mobs/Green_Mushroom/stand_0.png");
    greenMushroom_stand[1] = QString(":/Mobs/Mobs/Green_Mushroom/stand_1.png");
    greenMushroom_stand[2] = QString(":/Mobs/Mobs/Green_Mushroom/stand_2.png");

    /* --------------------------------------- Ribbon Pig Animations --------------------------------------- */
    // Ribbon Pig Move frames
    ribbonPig_move[0] = QString(":/Mobs/Mobs/Ribbon_Pig/move_0.png");
    ribbonPig_move[1] = QString(":/Mobs/Mobs/Ribbon_Pig/move_1.png");
    ribbonPig_move[2] = QString(":/Mobs/Mobs/Ribbon_Pig/move_2.png");

    // Ribbon Pig Stand frames
    ribbonPig_stand[0] = QString(":/Mobs/Mobs/Ribbon_Pig/stand_0.png");
    ribbonPig_stand[1] = QString(":/Mobs/Mobs/Ribbon_Pig/stand_1.png");
    ribbonPig_stand[2] = QString(":/Mobs/Mobs/Ribbon_Pig/stand_2.png");

    /* --------------------------------------- Stump Animations --------------------------------------- */
    // Stump Move frames
    stump_move[0] = QString(":/Mobs/Mobs/Stump/move_0.png");
    stump_move[1] = QString(":/Mobs/Mobs/Stump/move_1.png");
    stump_move[2] = QString(":/Mobs/Mobs/Stump/move_2.png");
    stump_move[3] = QString(":/Mobs/Mobs/Stump/move_3.png");

    // Stump Stand frames
    stump_stand[0] = QString(":/Mobs/Mobs/Stump/stand_0.png");

}
