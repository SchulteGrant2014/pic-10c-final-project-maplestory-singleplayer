#include "OrangeMushroom.h"
#include "Enemy.h"

extern Animations* animations;

OrangeMushroom::OrangeMushroom() : Enemy(animations->orangeMushroom_move[0], true, 5, 10) {
    frameCounter_move.setFrameTotal(animations->orangeMushroom_move.size());
    frameCounter_stand.setFrameTotal(animations->orangeMushroom_stand.size());
}

OrangeMushroom::~OrangeMushroom() {

}

void OrangeMushroom::updateFrame_move(size_t frameIndex) {
    setPixmap(animations->orangeMushroom_move[frameIndex]);
}

void OrangeMushroom::updateFrame_stand(size_t frameIndex) {
    setPixmap(animations->orangeMushroom_stand[frameIndex]);
}

void OrangeMushroom::updateFrame_jump() {
    setPixmap(QString(":/Mobs/Mobs/Orange_Mushroom/jump_0.png"));
}
