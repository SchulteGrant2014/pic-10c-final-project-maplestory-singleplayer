#ifndef ANIMATIONFRAMECOUNTER_H
#define ANIMATIONFRAMECOUNTER_H

#include <QString>
#include <vector>

class AnimationFrameCounter {
public:
    AnimationFrameCounter(size_t vecSize);
    AnimationFrameCounter& operator=(size_t i) { frameCurrent = i; return *this; }
    virtual ~AnimationFrameCounter();

    size_t& operator++();  // Prefix++
    size_t operator++(int);  // Postfix++
    size_t operator*();  // Dereferencing operator

    void setFrameCurrent(size_t i) { frameCurrent = i; }
    void setFrameTotal(size_t i) { frameTotal = i; }

private:
    size_t frameCurrent;
    size_t frameTotal;
};

#endif // ANIMATIONFRAMECOUNTER_H
