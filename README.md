# README #

This project is my final project for PIC 10C at UCLA, Winter 2017. I have attempted to re-create the MMORPG called "MapleStory", which is owned by Nexon. I mimic the player movement dynamics, as well as the monster movement AI. The player can walk along platforms, change directions, ascend and descend ladders, as well as jump.

This game differs from the online version in that one can "win" by eliminating 25 monsters, and one can "lose" by having his or her health go down to zero. A single blow to a monster will eliminate it. A single blow to the player will damage the player's health by some amount, depending on the type of monster and its "attack" stat. Upon being hit by a monster, the player cannot be hit again for another 1.5 seconds.

### YouTube Link of Project Demonstration: ###
https://www.youtube.com/watch?v=xBdCFbPeJfA&feature=youtu.be

### Controls: ###

* **Left**: Face and move left (unless on a ladder)
* **Right**: Face and move right (unless on a ladder)
* **Up**: Attach to / move up a ladder
* **Down**: Move down a ladder (can attach to ladder using "Down" only if on platform at top of ladder)
* **Space**: Shoot a projectile
* **Shift**: Jump (unless already in the air)
* **K**: (cheat) Kill all monsters on the map

**This project attempts to use as many components of the course (PIC 10C) as possible. Here are a list of the course-specific material found in my project [there may be more, but this is what I remember]:**

**1) Source Control**

I use Git and BitBucket to keep track of my changes, make new branches, merge changes, etc.

**2) Qt**

This program makes use of Qt by utilizing its graphics, music, and user interface functionalities.

**3) Memory Management**

Any classes that manage memory on the heap have Copy-Swap based implementations. All "new" calls are encased in try-catch (such as each time a monster is spawned through "void Game::spawnSingleEnemy()". All destructors are "virtual". Destructors free heap memory.

Bullet, Enemy, and MyRect classes manage memory and have implementations of Copy-Swap: Default constructor, Copy constructor, Move constructor, Assignment operator, member Swap function.

The Game class uses "= delete" next to any constructors/functions that can copy the Game object, as there should only ever be one Game.

**4) Inheritance + Polymorphism**

All of the different monsters inherit from a base "Enemy" class. This way, an Enemy* pointer can be used to traverse the objects in the scene and use "dynamic_cast" to determine if the scene object is some form of enemy, generally.

**5) Function Pointers**

The "MyRect" (player) class has a member function pointer "void (*moveLogic_ladder)(MyRect*);" which points to a function containing the move logic of the player when he/she is on a ladder (not on a platform). This function is called in the SLOT: "void MyRect::move()" which encases the player movement instructions for both when he/she is on a ladder or on some platform.

**6) Functors / Function Objects**

The "MyRect" (player) class and "Enemy" class also have member functors "MoveLogicFunctor<MyRect> moveLogicFnctr;" which have "operator()()" overloaded to contain the movement logic of the player when he/she is on a platform (not on a ladder). This function is also called in the SLOT: "void MyRect::move()" which encases the player movement instructions for both when he/she is on a ladder or on some platform.

**7) Templates**

The Functor "MoveLogicFunctor<MyRect> moveLogicFnctr;" described above is templated so that it can work with many classes. I specifically use the functor for both the "MyRect" (player) class and the "Enemy" class, as both the player and the enemies have the same movement logic.

**8) Iterators**

Every time the program checks to see if the player (MyRect) or a Bullet collide with an enemy by using the collidingItems() function, a QList<QGraphicsItem*> is returned. I navigate these containers using iterators and while-loops. There are also many other varied uses of iterators, for example in the "void Game::pause()" function.

I also make use of iterators in my Lambda Function and generic algorithm for_each(), described in the "Lambda Functions" section below.

**9) Lambda Functions + Generic Algorithms**

When the key "K" is pressed, all monsters on the screen are eliminated. The function "void killAllMonsters();" is found in the Game class. The function navigates the vector of scene enemies using the generic algorithm "std::for_each(...)" using iterators to access each index and using a Lambda Function to remove the enemy from the scene and delete it from the heap.