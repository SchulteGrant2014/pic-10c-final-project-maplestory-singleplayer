#include "GreenMushroom.h"
#include "Enemy.h"

extern Animations* animations;

GreenMushroom::GreenMushroom() : Enemy(animations->greenMushroom_move[0], false, 4, 20) {
    frameCounter_move.setFrameTotal(animations->greenMushroom_move.size());
    frameCounter_stand.setFrameTotal(animations->greenMushroom_stand.size());
}

GreenMushroom::~GreenMushroom() {

}

void GreenMushroom::updateFrame_move(size_t frameIndex) {
    setPixmap(animations->greenMushroom_move[frameIndex]);
}

void GreenMushroom::updateFrame_stand(size_t frameIndex) {
    setPixmap(animations->greenMushroom_stand[frameIndex]);
}

void GreenMushroom::updateFrame_jump() {
    // Can't jump... just set to standing
    setPixmap(QString(":/Mobs/Mobs/Green_Mushroom/stand_0.png"));
}
