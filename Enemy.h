#ifndef ENEMY_H
#define ENEMY_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QTimer>
#include <vector>
#include "MyRect.h"
#include "Platform.h"
#include "MoveLogicFunctor.h"
#include "Animations.h"
#include "AnimationFrameCounter.h"

class Enemy : public QObject, public QGraphicsPixmapItem {

    Q_OBJECT

public:
    Enemy(QString filePath, bool canJump, int speed, int attack);  // Normal Constructor
    Enemy(const Enemy& a);  // Copy Constructor
    Enemy(Enemy&& a);  // Move Constructor
    //Enemy& operator=(Enemy a);  // Assignment Operator  --> PURE VIRTUAL CLASS... CANNOT IMPLEMENT WITH COPY-SWAP
    void swap(Enemy& a);
    virtual ~Enemy();

    void stopMoveDecisionTimer();
    void startMoveDecisionTimer(int msec);
    void stopMoveTimer();
    void startMoveTimer(int msec);

    void changeVelocityRand();
    void changeVelocity(int velX, int velY);
    void jump();
    void set_moving(bool a) { moving = a; }
    bool get_moving() { return moving; }
    Platform*& get_platformStandingOn() { return platformStandingOn; }

    virtual void updateFrame_move(size_t frameIndex) = 0;
    virtual void updateFrame_stand(size_t frameIndex) = 0;
    virtual void updateFrame_jump() = 0;

    void inflictDamage();

public slots:
    void move();
    void decideChangeVelocity();


protected:
    bool canJump;  // Not all enemies have the ability to jump
    int speed;  // Different enemies can move at different speeds
    int attack; // Different enemies have different attack stats

private:
    QTimer* moveDecisionTimer;
    QTimer* moveTimer;
    Platform* platformStandingOn;  // Memory managed by Qt
    MoveLogicFunctor<Enemy> moveLogicFnctr;
    float velocityX;
    float velocityY;
    bool moving;

protected:
    AnimationFrameCounter frameCounter_move;
    AnimationFrameCounter frameCounter_stand;
    size_t animationBuffer;

    friend class MoveLogicFunctor<Enemy>;
};


#endif // ENEMY_H
