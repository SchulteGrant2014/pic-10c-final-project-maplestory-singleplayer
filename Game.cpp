
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QMediaPlayer>
#include <QBrush>
#include <QImage>
#include <QList>
#include <algorithm>
#include <vector>
#include "Game.h"
#include "Enemy.h"
#include "MyRect.h"
#include "Slime.h"
#include "RibbonPig.h"
#include "OrangeMushroom.h"
#include "GreenMushroom.h"
#include "Stump.h"
#include "Platform.h"
#include <cstdlib>

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

Game::Game() : player(nullptr), scene(nullptr), view(nullptr), spawnTimer(nullptr), music(nullptr), score(nullptr), health(nullptr), gameOverText(nullptr) {

    // Seed the rand() function
    std::srand(time(nullptr));

    // Create a SCENE...
    try {
        scene = new QGraphicsScene;
    } catch (std::exception& e) {
        delete scene;
        scene = nullptr;
        throw;
    }

    scene->setSceneRect(0,0,800, 600);  // Set the scene to have fixed size so it doesn't expand indefinitely.
    scene->setBackgroundBrush(QBrush(QImage(":/sounds/Background_Henesys.png")));

    // Add a VIEW to the scene so that we can visualize our items...
    try {
        view = new QGraphicsView(scene);
    } catch (std::exception& e) {
        delete view;
        view = nullptr;
        throw;
    }

    view->setFixedSize(800, 600);  // Set the size of the VIEW. Note, the Scene size is unchanged.
    view->setWindowTitle("Test Project");
    view->show();

    // ADD PLATFORMS/ground to stand on...
    Platform* ground = add_platform_toScene(0, 520, SCREEN_WIDTH, 80);  // Ground
    add_platform_toScene(320, 466, 190, 52);  // Lower haystacks
    add_platform_toScene(372, 408, 102, 52);  // Upper haystack
    add_platform_toScene(0, 238, 472, 48);    // Left top platform
    add_platform_toScene(604, 238, 800, 48);  // Right top platform
    add_platform_toScene(136, 348, 204, 36);  // Shack roof
    add_ladder_toScene(378, 236, 52, 118);  // Ladder (Left)
    add_ladder_toScene(642, 236, 52, 232);  // Ladder (Right)

    // Set all platforms and ladders to be invisible
    {
        std::vector<Platform*>::iterator beginPlatforms = platforms.begin();
        std::vector<Platform*>::iterator endPlatforms = platforms.end();
        while (beginPlatforms != endPlatforms) {
            (*beginPlatforms)->setOpacity(0);
            ++beginPlatforms;
        }
    }
    {
        std::vector<Ladder*>::iterator beginLadders = ladders.begin();
        std::vector<Ladder*>::iterator endLadders = ladders.end();
        while (beginLadders != endLadders) {
            (*beginLadders)->setOpacity(0);
            ++beginLadders;
        }
    }

    // Create an ITEM to put into the scene...  // NOTE: Pixmap items use "boundingRect()" instead of "rect()" to get item dimensions
    try {
        player = new MyRect;
    } catch (std::exception& e) {
        delete player;
        player = nullptr;
        throw;
    }

    player->setPos( ((view->width() / 2) - (player->boundingRect().width() / 2)), (view->height() - ground->rect().height() - player->boundingRect().height() - 20) );

    // ADD the item to the scene...
    scene->addItem(player);

    // Only one item at a time can respond to keyboard events. Make this item the one to respond to the keyboard events.
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();

    // Add SCORE + HEALTH
    try {
        score = new Score();
    } catch (std::exception& e) {
        delete score;
        score = nullptr;
        throw;
    }

    scene->addItem(score);

    try {
        health = new Health();
    } catch (std::exception& e) {
        delete health;
        health = nullptr;
        throw;
    }

    scene->addItem(health);

    // Add MUSIC + SOUND
    try {
        music = new QMediaPlayer;
    } catch (std::exception& e) {
        delete music;
        music = nullptr;
        throw;
    }

    music->setMedia(QUrl("qrc:/sounds/bgm.mp3"));
    music->play();

}

Game::~Game() {
    delete player;         player = nullptr;
    delete scene;          scene = nullptr;
    delete view;           view = nullptr;
    delete spawnTimer;     spawnTimer = nullptr;
    delete music;          music = nullptr;
    // Note... Scene calls delete on gameOverText and all platforms[i]
}



void Game::spawnEnemy_slot() {
    for (size_t i = 0, numEnemies = 3; i < numEnemies; ++i) {
        spawnSingleEnemy();
    }
}

void Game::spawnSingleEnemy() {
    Enemy* enemy = nullptr;
    int enemyType = rand() % 5;
    switch (enemyType) {
    case 0:
        try {
            enemy = new Slime;
        } catch (std::exception& e) {
            delete enemy;
            enemy = nullptr;
            throw;
        }

        break;
    case 1:
        try {
            enemy = new RibbonPig;
        } catch (std::exception& e) {
            delete enemy;
            enemy = nullptr;
            throw;
        }
        break;
    case 2:
        try {
            enemy = new OrangeMushroom;
        } catch (std::exception& e) {
            delete enemy;
            enemy = nullptr;
            throw;
        }
        break;
    case 3:
        try {
            enemy = new GreenMushroom;
        } catch (std::exception& e) {
            delete enemy;
            enemy = nullptr;
            throw;
        }
        break;
    case 4:
        try {
            enemy = new Stump;
        } catch (std::exception& e) {
            delete enemy;
            enemy = nullptr;
            throw;
        }
        break;
    }
    enemies.push_back(enemy);
    scene->addItem(enemy);

}

void Game::killAllEnemies() {
    // Use a LAMBDA FUNCTION to remove each enemy from the scene and delete it from heap memory...
    if (enemies.size() != 0) {
        std::for_each(enemies.begin(), enemies.end(),
                      [this](Enemy* currentEnemy){
                            this->scene->removeItem(currentEnemy);
                            delete currentEnemy;
                            currentEnemy = nullptr;
                      }
        );
        enemies = {};
    }
}

void Game::pause() {
    stopSpawnTimer();
    player->stopMoveTimer();
    player->stopEnemyCollisionTimer();

    QList<QGraphicsItem*> sceneItems = scene->items();
    QList<QGraphicsItem*>::iterator beginItems = sceneItems.begin();
    QList<QGraphicsItem*>::iterator endItems = sceneItems.end();
    while (beginItems != endItems) {
        Enemy* potential_enemy = dynamic_cast<Enemy*>(*beginItems);
        if (potential_enemy != nullptr) {
            potential_enemy->stopMoveTimer();
            potential_enemy->stopMoveDecisionTimer();
        }
        ++beginItems;
    }

}

void Game::startSpawnTimer(int msec) {
    // If timer already exists, get rid of it and make a new one
    delete spawnTimer;

    // Create enemies periodically
    try {
        spawnTimer = new QTimer;
    } catch (std::exception& e) {
        delete spawnTimer;
        spawnTimer = nullptr;
        throw;
    }

    QObject::connect(spawnTimer, SIGNAL(timeout()), this, SLOT(spawnEnemy_slot()));
    spawnTimer->start(msec);
}


void Game::stopSpawnTimer() {
    delete spawnTimer;
    spawnTimer = nullptr;
}

Platform* Game::add_platform_toScene(int x, int y, int w, int h) {
    Platform* p = nullptr;
    try {
        p = new Platform(x, y, w, h);
    } catch (std::exception& e) {
        delete p;
        p = nullptr;
        throw;
    }

    scene->addItem(p);
    platforms.push_back(p);

    return p;
}

Ladder* Game::add_ladder_toScene(int x, int y, int w, int h) {
    Ladder* ldr = nullptr;
    try {
        ldr = new Ladder(x, y, w, h);
    } catch (std::exception& e) {
        delete ldr;
        ldr = nullptr;
        throw;
    }

    scene->addItem(ldr);
    return ldr;
}

void Game::increase_score(int n) {
    score->increase_score(n);
    if (score->get_score() >= 25) {
        gameOver(true);
    }
}

void Game::decrease_health(int n) {
    health->decrease_health(n);
    if (health->get_health() <= 0) {
        gameOver(false);
    }
}

void Game::gameOver(bool won) {
    QString message;
    if (won) {
        message = "Congratulations! You won!";
    } else {
        message = "Game Over!";
    }

    displayGameOverWindow(message);
}

void Game::displayGameOverWindow(QString message) {

    // Disable all of the objects in the scene so everything stops
    pause();

    // Display the Game Over message
    try {
        gameOverText = new QGraphicsTextItem(message);
    } catch (std::exception& e) {
        delete gameOverText;
        gameOverText = nullptr;
        throw;
    }

    gameOverText->setFont(QFont("Times", 48));
    gameOverText->setDefaultTextColor(Qt::red);
    float x_pos = (SCREEN_WIDTH / 2) - (gameOverText->boundingRect().width() / 2);
    float y_pos = (SCREEN_HEIGHT / 3);
    gameOverText->setPos(x_pos, y_pos);
    scene->addItem(gameOverText);  // Scene calls delete at program termination

}


