#include "RibbonPig.h"
#include "Enemy.h"

extern Animations* animations;

RibbonPig::RibbonPig() : Enemy(animations->ribbonPig_move[0], true, 5, 15) {
    frameCounter_move.setFrameTotal(animations->ribbonPig_move.size());
    frameCounter_stand.setFrameTotal(animations->ribbonPig_stand.size());
}

RibbonPig::~RibbonPig() {

}

void RibbonPig::updateFrame_move(size_t frameIndex) {
    setPixmap(animations->ribbonPig_move[frameIndex]);
}

void RibbonPig::updateFrame_stand(size_t frameIndex) {
    setPixmap(animations->ribbonPig_stand[frameIndex]);
}

void RibbonPig::updateFrame_jump() {
    setPixmap(QString(":/Mobs/Mobs/Ribbon_Pig/jump_0.png"));
}
