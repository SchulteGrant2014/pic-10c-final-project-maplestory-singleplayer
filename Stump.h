#ifndef STUMP_H
#define STUMP_H

#include "Enemy.h"

class Stump : public Enemy {
public:
    Stump();
    virtual ~Stump();

    virtual void updateFrame_move(size_t frameIndex);
    virtual void updateFrame_stand(size_t frameIndex);
    virtual void updateFrame_jump();
};

#endif // STUMP_H
