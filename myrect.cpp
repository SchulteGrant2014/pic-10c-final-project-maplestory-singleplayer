
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QTimer>
#include <QList>
#include "Game.h"
#include "MyRect.h"
#include "Bullet.h"
#include "Enemy.h"
#include "Platform.h"
#include "Ladder.h"

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern Game* game;


/** This function encapsulates the player movement logic while on a ladder.
 * It is assumed that this function is being called when "onLadder" == true.
 *
 * Requires the following member variables:
 * (1) Ladder* ladderStandingOn;
 *
 */
void ladderMoveLogic(MyRect* player) {

    player->velocityX = 0;  // On a ladder, the player can only move up and down

    float y_current = player->pos().y();
    float y_new = y_current + player->velocityY;

    // Test if player movement will take him/her off of the ladder
    if (player->velocityY < 0 && player->ladderStandingOn->atTop(player)) {
        player->ladderStandingOn = nullptr;
    } else if (player->velocityY > 0 && player->ladderStandingOn->atBottom(player)) {
        player->ladderStandingOn = nullptr;
    } else {
        player->setPos(player->pos().x(), y_new);
    }

}




MyRect::MyRect() : QObject(), QGraphicsPixmapItem(), velocityX(0), velocityY(0), directionFacing(0), moveTimer(nullptr), enemyCollisionTimer(nullptr), collisionCooldownTimer(nullptr), moving(false), collisionCooldown(false), platformStandingOn(nullptr), ladderStandingOn(nullptr), moveLogicFnctr(MoveLogicFunctor<MyRect>()), moveLogic_ladder(ladderMoveLogic) {
    // Note: Player starts slightly in the air when spawning
    // Set pixmap of player
    setPixmap(QString(":/images/Mage_Alert_1.png"));

    // Set up a timer to control movement
    startMoveTimer(35);

    // Set up a timer to check for collisions with enemies and cause damage if a collision exists
    startEnemyCollisionTimer(200);
}

MyRect::MyRect(const MyRect &a) {
    // Copy over stack member variables
    velocityX = a.velocityX;
    velocityY = a.velocityY;
    keysDown = a.keysDown;
    directionFacing = a.directionFacing;
    moveTimer_refreshRate = a.moveTimer_refreshRate;
    moving = a.moving;
    collisionCooldown = a.collisionCooldown;
    platformStandingOn = a.platformStandingOn;  // Memory handled by Qt
    ladderStandingOn = a.ladderStandingOn;      // Memory handled by Qt
    moveLogicFnctr = a.moveLogicFnctr;
    moveLogic_ladder = a.moveLogic_ladder;

    // Manage heap memory...
    try {
        moveTimer = new QTimer;
    } catch (std::exception& e) {
        delete moveTimer;
        moveTimer = nullptr;
        throw;
    }
    try {
        enemyCollisionTimer = new QTimer;
    } catch (std::exception& e) {
        delete enemyCollisionTimer;
        enemyCollisionTimer = nullptr;
        throw;
    }
    try {
        collisionCooldownTimer = new QTimer;
    } catch (std::exception& e) {
        delete collisionCooldownTimer;
        collisionCooldownTimer = nullptr;
        throw;
    }
}

MyRect::MyRect(MyRect &&a) {
    this->swap(a);
}

MyRect& MyRect::operator=(MyRect a) {
    this->swap(a);
    return *this;
}

void MyRect::swap(MyRect& a) {
    std::swap(velocityX, a.velocityX);
    std::swap(velocityY, a.velocityY);
    std::swap(keysDown, a.keysDown);
    std::swap(directionFacing, a.directionFacing);
    std::swap(moveTimer, a.moveTimer);
    std::swap(enemyCollisionTimer, a.enemyCollisionTimer);
    std::swap(collisionCooldownTimer, a.collisionCooldownTimer);
    std::swap(moveTimer_refreshRate, a.moveTimer_refreshRate);
    std::swap(moving, a.moving);
    std::swap(collisionCooldown, a.collisionCooldown);
    std::swap(platformStandingOn, a.platformStandingOn);
    std::swap(ladderStandingOn, a.ladderStandingOn);
    std::swap(moveLogicFnctr, a.moveLogicFnctr);
    std::swap(moveLogic_ladder, a.moveLogic_ladder);
}

MyRect::~MyRect() {
    delete moveTimer;              moveTimer = nullptr;
    delete enemyCollisionTimer;    enemyCollisionTimer = nullptr;
    delete collisionCooldownTimer; collisionCooldownTimer = nullptr;
}

void MyRect::keyPressEvent(QKeyEvent *e) {

    int key_pressed = e->key();
    keysDown.insert(key_pressed);  // Note: A set does not store duplicates

    if (key_pressed == Qt::Key_Left) {
        velocityX = -5;
        directionFacing = 0;  // Set facing 0 = Left
    } else if (key_pressed == Qt::Key_Right) {
        velocityX = 5;
        directionFacing = 1;  // Set facing 1 = Right
    } else if (key_pressed == Qt::Key_Shift) {  // If not already in the air, jump when instructed...
        if (ladderStandingOn == nullptr) {
            if (platformStandingOn != nullptr) {
                jump();
            }
        } else {
            velocityY = -8;
            ladderStandingOn = nullptr;
        }
    } else if (key_pressed == Qt::Key_Up) {
        if (ladderStandingOn == nullptr) {  // If not on a ladder but Up pressed, check and see if there is a ladder to attach to
            QList<QGraphicsItem*> collisions = collidingItems();
            QList<QGraphicsItem*>::iterator beginCollisions = collisions.begin();
            QList<QGraphicsItem*>::iterator endCollisions = collisions.end();
            while (beginCollisions != endCollisions) {
                Ladder* ladderCurrentlyOn = dynamic_cast<Ladder*>(*beginCollisions);
                int playerFeetY_minusFive = this->pos().y() + this->boundingRect().height() - 5;
                if ( ladderCurrentlyOn != nullptr && playerFeetY_minusFive > ladderCurrentlyOn->pos().y() ) {  // If not already at the top of the ladder, attach!
                    ladderStandingOn = ladderCurrentlyOn;
                    platformStandingOn = nullptr;
                    velocityY = -5;
                    int pos_ladder_midX = ladderCurrentlyOn->pos().x() + (ladderCurrentlyOn->rect().width() / 2) - (this->boundingRect().width() / 2);
                    setPos(pos_ladder_midX, y());
                }
                ++beginCollisions;
            }
        } else {  // If already attached to a ladder, control movement using arrow keys
            velocityY = -5;
        }
    } else if (key_pressed == Qt::Key_Down) {
        if (ladderStandingOn != nullptr) {  // If on a ladder in the air, then descend the ladder
            velocityY = 5;
        } else if (platformStandingOn != nullptr) {  // If on a platform AND the player's FEET are touching a ladder, go down the ladder
            QList<QGraphicsItem*> collisions = collidingItems();
            QList<QGraphicsItem*>::iterator beginCollisions = collisions.begin();
            QList<QGraphicsItem*>::iterator endCollisions = collisions.end();
            while (beginCollisions != endCollisions) {
                Ladder* ladderCurrentlyOn = dynamic_cast<Ladder*>(*beginCollisions);
                if (ladderCurrentlyOn != nullptr) {
                    int playerFeet = pos().y() + this->boundingRect().height();
                    int ladderTop = ladderCurrentlyOn->pos().y() + ladderCurrentlyOn->boundingRect().height();
                    if (playerFeet <= ladderTop) {  // If the player's FEET are at all below the top of the ladder, climb on the ladder
                        ladderStandingOn = ladderCurrentlyOn;
                        platformStandingOn = nullptr;
                        velocityY = 5;
                        int pos_ladder_midX = ladderCurrentlyOn->pos().x() + (ladderCurrentlyOn->rect().width() / 2) - (this->boundingRect().width() / 2);
                        setPos(pos_ladder_midX, y());
                    }
                }
                ++beginCollisions;
            }
        }
    } else if (key_pressed == Qt::Key_Space) {
        // Create a Bullet on the heap...
        Bullet* b = nullptr;
        try {
            b = new Bullet(directionFacing);
        } catch (std::exception& e) {
            delete b;
            b = nullptr;
            throw;
        }
        // Add the new Bullet to the scene... scene() returns a reference to the object's scene: #include <QGraphicsScene>
        this->scene()->addItem(b);
        b->setPos(this->x(), this->y());  // Create the bullet at the location of the player [in case player not at (0,0)]
    } else if (key_pressed == Qt::Key_K) {
        game->killAllEnemies();
    }
}

void MyRect::keyReleaseEvent(QKeyEvent *e) {

    int key_released = e->key();
    keysDown.erase(key_released);

    std::set<int>::iterator endIter = keysDown.end();

    if (key_released == Qt::Key_Left) {
        if (keysDown.find(Qt::Key_Right) != endIter) {  // If released left but right still pressed
            velocityX = 5;
            directionFacing = 1;
        } else {
            this->velocityX = 0;
        }
    } else if (key_released == Qt::Key_Right) {
        if (keysDown.find(Qt::Key_Left) != endIter) {
            velocityX = -5;
            directionFacing = 0;
        } else {
            this->velocityX = 0;
        }
    } else if (key_released == Qt::Key_Up && ladderStandingOn != nullptr) {
        if (keysDown.find(Qt::Key_Down) != endIter) {
            velocityY = -5;
        } else {
            this->velocityY = 0;
        }
    } else if (key_released == Qt::Key_Down && ladderStandingOn != nullptr) {
        if (keysDown.find(Qt::Key_Up) != endIter) {
            velocityY = -5;
        } else {
            this->velocityY = 0;
        }
    }
}

void MyRect::stopMoveTimer() {
    delete moveTimer;
    moveTimer = nullptr;
}

void MyRect::startMoveTimer(int msec) {
    // If timer already exists, get rid of it and make a new one
    delete moveTimer;

    // Create enemies periodically
    try {
        moveTimer = new QTimer;
    } catch (std::exception& e) {
        delete moveTimer;
        moveTimer = nullptr;
        throw;
    }

    QObject::connect(moveTimer, SIGNAL(timeout()), this, SLOT(move()));
    moveTimer->start(msec);
}

void MyRect::stopEnemyCollisionTimer() {
    delete enemyCollisionTimer;
    enemyCollisionTimer = nullptr;
}

void MyRect::startEnemyCollisionTimer(int msec) {
    // If timer already exists, get rid of it and make a new one
    delete enemyCollisionTimer;

    // Check for collisions with enemies periodically
    try {
        enemyCollisionTimer = new QTimer;
    } catch (std::exception& e) {
        delete enemyCollisionTimer;
        enemyCollisionTimer = nullptr;
        throw;
    }

    QObject::connect(enemyCollisionTimer, SIGNAL(timeout()), this, SLOT(checkForEnemyCollision()));
    enemyCollisionTimer->start(msec);
}

void MyRect::jump(){
    velocityY = -15;
    platformStandingOn = nullptr;
}

void MyRect::move() {
    if (ladderStandingOn != nullptr) {  // If on a ladder, call ladder movement logic function
        moveLogic_ladder(this);
    } else {  // If on ground, call platform movement logic function
        moveLogicFnctr(*this);  // Call functor containing movement logic to update position
    }
}

void MyRect::checkForEnemyCollision() {
    if (!collisionCooldown) {  // If not on damage cooldown, check for enemy collisions
        QList<QGraphicsItem*> collisions = collidingItems();
        auto begin = collisions.begin();
        auto end = collisions.end();
        while (begin != end) {
            Enemy* enemy_colliding = dynamic_cast<Enemy*>(*begin);
            if (enemy_colliding != nullptr) {
                enemy_colliding->inflictDamage();  // Enemy does damage based on its attack stat
                startCollisionCooldown();
                return;
            }
            ++begin;
        }
    }
}

void MyRect::endCollisionCooldown() {
    collisionCooldown = false;
    delete collisionCooldownTimer;
    collisionCooldownTimer = nullptr;
}

void MyRect::startCollisionCooldown() {
    try {
        collisionCooldownTimer = new QTimer;
    } catch (std::exception& e) {
        delete collisionCooldownTimer;
        collisionCooldownTimer = nullptr;
        throw;
    }

    collisionCooldown = true;  // Most importantly, record that the player is on collision cooldown!

    QObject::connect(collisionCooldownTimer, SIGNAL(timeout()), this, SLOT(endCollisionCooldown()));
    collisionCooldownTimer->start(1500);  // After being hit, player can't be hit again for 1.5 seconds
}




